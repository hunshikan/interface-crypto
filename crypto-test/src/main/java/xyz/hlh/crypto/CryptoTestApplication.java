package xyz.hlh.crypto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description: 启动类
 * @date : Created in 2022/2/4 9:15
 */
@SpringBootApplication
public class CryptoTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(CryptoTestApplication.class, args);
    }
}

# 项目接口加密

#### 介绍
springboot的接口自动加密和解密,通过ControllerAdvice 拦截Controller 进行加解密
1.和其他系统的业务交互，或者微服务之间的接口调用
2.保证数据传输的安全，对接口出参加密，入参解密。


#### 使用说明

1.获取加密数据

![输入图片说明](https://foruda.gitee.com/images/1669896478885939941/de164fad_1784941.png "屏幕截图")

2.解密接口
![输入图片说明](https://foruda.gitee.com/images/1669896524280549752/d360b54d_1784941.png "屏幕截图")
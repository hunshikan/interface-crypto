package xyz.hlh.crypto.common.exception;


public class CryptoException extends CustomizeException {

    public CryptoException(String message, Throwable cause) {
        super(message, cause);
    }

    public CryptoException(String message) {
        super(message);
    }

}

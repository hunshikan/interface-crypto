package xyz.hlh.crypto.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * @description:
 * @date : Created in 2022/2/4 15:48
 */
@Getter
@Setter
@EqualsAndHashCode
public class RequestBase {

    private Long currentTimeMillis;

}
